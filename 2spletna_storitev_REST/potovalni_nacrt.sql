-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 05. sep 2023 ob 15.14
-- Različica strežnika: 10.4.28-MariaDB
-- Različica PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `potovalni_nacrt`
--

-- --------------------------------------------------------

--
-- Struktura tabele `destinacija`
--

CREATE TABLE `destinacija` (
  `IDdestinacije` int(11) NOT NULL,
  `ime_destinacije` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_general_ci;

--
-- Odloži podatke za tabelo `destinacija`
--

INSERT INTO `destinacija` (`IDdestinacije`, `ime_destinacije`) VALUES
(1, 'London'),
(2, 'Kankun'),
(3, 'Dubai'),
(4, 'Bali'),
(5, 'Kreta'),
(6, 'Rim'),
(7, 'Rt Svete Luke'),
(8, 'Malorka'),
(9, 'Instabul'),
(10, 'Paris'),
(11, 'Hurgada'),
(12, 'Agadir'),
(13, 'Peking');

-- --------------------------------------------------------

--
-- Struktura tabele `oseba`
--

CREATE TABLE `oseba` (
  `vzdevek` varchar(20) NOT NULL,
  `geslo` varchar(32) NOT NULL,
  `ime` varchar(20) NOT NULL,
  `priimek` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `vloga` int(1) NOT NULL COMMENT 'tip_prijave\r\n1:admin\r\n2:agencija\r\n3:uporabnik'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_general_ci;

--
-- Odloži podatke za tabelo `oseba`
--

INSERT INTO `oseba` (`vzdevek`, `geslo`, `ime`, `priimek`, `email`, `vloga`) VALUES
('admin', 'a66abb5684c45962d887564f08346e8d', 'David', 'Legan', 'dlegan@gmail.com', 1),
('agen', '3f0df78b0d049be1f7a59586161c366e', 'Mateja', 'Kogoj', 'palma89@gmail.com', 2),
('krneki', '$2y$10$c6H3fW8DQMLtG', 'Krnegis', 'Abudal', 'krnab@gmail.com', 3),
('lukas77', 'lookah', 'Luka', 'Novak', 'novakluka1357@jahu.com', 3),
('mojca', 'abdc123', 'Mojca', 'Novak', 'mojca_novak82@mail.com', 3),
('upor', '745dfa1740d12b0628f34906cd4e1db5', 'Marko', 'Skace', 'marska@gmail.com', 3);

-- --------------------------------------------------------

--
-- Struktura tabele `potovanje`
--

CREATE TABLE `potovanje` (
  `IDpotovanja` int(11) NOT NULL,
  `IDdestinacije` int(11) NOT NULL,
  `datum` date NOT NULL COMMENT 'datum termina',
  `trajanje` int(11) NOT NULL COMMENT 'dnevi',
  `agencija` varchar(20) NOT NULL,
  `cena` int(11) NOT NULL COMMENT '€',
  `opis_aranzmaja` text NOT NULL COMMENT 'program',
  `casovni_zig` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_general_ci;

--
-- Odloži podatke za tabelo `potovanje`
--

INSERT INTO `potovanje` (`IDpotovanja`, `IDdestinacije`, `datum`, `trajanje`, `agencija`, `cena`, `opis_aranzmaja`, `casovni_zig`) VALUES
(10, 1, '2023-08-10', 5, 'Potovanje.si', 2000, 'Zdruzeno Kraljestvo: London\r\n\r\nLokacija in okolica:\r\n500m do barov, nočnih klubov, restavracij\r\n\r\nRazdalje:\r\n150 m do plaže\r\n\r\nNastanitev:\r\n2 posteljna\r\n\r\nHrana in pijača:\r\nvključen samopostrežni zajtrk\r\n\r\nVodni športi:\r\nsurfanje\r\n\r\nUgodnosti in prednosti:\r\nmasaže', '2023-01-08 14:25:53'),
(11, 2, '2023-03-17', 14, 'Oasisturs', 1400, '', '2023-01-08 14:26:18'),
(12, 3, '2023-04-26', 13, 'Oasistours', 1400, '', '2023-01-08 14:26:24'),
(13, 4, '2023-04-29', 10, 'Odpelji.se', 1100, '', '2023-01-08 14:26:30'),
(14, 5, '2023-07-11', 14, 'Potovanje.si', 1350, '', '2023-01-08 14:26:37'),
(15, 6, '2023-08-01', 14, 'Odpelji.se', 1600, '', '2023-01-08 14:26:43'),
(16, 7, '2023-05-01', 13, 'Oasistours', 1200, '', '2023-01-08 14:26:50'),
(17, 8, '2023-06-26', 17, 'Odpelji.se', 1700, '', '2023-01-08 14:26:57'),
(18, 9, '2023-08-10', 8, 'Odpelji.se', 1300, '', '2023-01-08 14:27:03'),
(19, 10, '2023-07-08', 7, 'Potovanje.si', 1900, '', '2023-01-08 14:27:14'),
(20, 11, '2023-03-04', 7, 'Oasistours', 1700, '', '2023-01-08 14:27:21'),
(21, 12, '2023-04-22', 5, 'Oasistours', 1500, '', '2023-01-08 14:27:29'),
(43, 13, '2022-02-01', 181, 'Potovanje.si', 1000, 'razdalja_plaze 150m', '2023-01-08 14:27:36'),
(44, 13, '2022-02-01', 181, 'Potovanje.si', 1000, 'razdalja_plaze 150m', '2023-01-08 14:27:42'),
(45, 13, '2022-02-01', 181, 'Potovanje.si', 1000, 'razdalja_plaze 150m', '2023-01-08 14:27:48');

-- --------------------------------------------------------

--
-- Struktura tabele `rezervacija`
--

CREATE TABLE `rezervacija` (
  `IDrezervacije` int(11) NOT NULL,
  `vzdevek` varchar(20) NOT NULL,
  `IDpotovanja` int(11) NOT NULL,
  `casovni_zig` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_general_ci;

--
-- Odloži podatke za tabelo `rezervacija`
--

INSERT INTO `rezervacija` (`IDrezervacije`, `vzdevek`, `IDpotovanja`, `casovni_zig`) VALUES
(3, 'mojca', 10, '2023-01-08 14:31:05'),
(4, 'krneki', 10, '2023-01-08 14:31:10'),
(5, 'lukas77', 17, '2023-01-08 14:30:26'),
(10, 'lukas77', 14, '2023-01-12 08:57:15'),
(18, 'mojca', 13, '2023-01-08 17:44:57'),
(19, 'lukas77', 10, '2023-01-08 17:51:35'),
(24, 'mojca', 20, '2023-01-08 18:01:10'),
(25, 'mojca', 17, '2023-01-12 09:08:53'),
(26, 'lukas77', 17, '2023-01-12 09:11:23'),
(28, 'lukas77', 45, '2023-01-12 09:41:03');

-- --------------------------------------------------------

--
-- Struktura tabele `tip_prijave`
--

CREATE TABLE `tip_prijave` (
  `vloga` int(1) NOT NULL,
  `opis` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_general_ci;

--
-- Odloži podatke za tabelo `tip_prijave`
--

INSERT INTO `tip_prijave` (`vloga`, `opis`) VALUES
(1, 'admin'),
(2, 'agencija'),
(3, 'uporabnik');

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `destinacija`
--
ALTER TABLE `destinacija`
  ADD PRIMARY KEY (`IDdestinacije`);

--
-- Indeksi tabele `oseba`
--
ALTER TABLE `oseba`
  ADD PRIMARY KEY (`vzdevek`),
  ADD KEY `vloga` (`vloga`),
  ADD KEY `IDvloge` (`vloga`),
  ADD KEY `vloga_2` (`vloga`);

--
-- Indeksi tabele `potovanje`
--
ALTER TABLE `potovanje`
  ADD PRIMARY KEY (`IDpotovanja`),
  ADD KEY `IDdestinacije` (`IDdestinacije`);

--
-- Indeksi tabele `rezervacija`
--
ALTER TABLE `rezervacija`
  ADD PRIMARY KEY (`IDrezervacije`),
  ADD KEY `vzdevek` (`vzdevek`),
  ADD KEY `IDpotovanja` (`IDpotovanja`);

--
-- Indeksi tabele `tip_prijave`
--
ALTER TABLE `tip_prijave`
  ADD PRIMARY KEY (`vloga`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `destinacija`
--
ALTER TABLE `destinacija`
  MODIFY `IDdestinacije` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT tabele `potovanje`
--
ALTER TABLE `potovanje`
  MODIFY `IDpotovanja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT tabele `rezervacija`
--
ALTER TABLE `rezervacija`
  MODIFY `IDrezervacije` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Omejitve tabel za povzetek stanja
--

--
-- Omejitve za tabelo `oseba`
--
ALTER TABLE `oseba`
  ADD CONSTRAINT `oseba_ibfk_1` FOREIGN KEY (`vloga`) REFERENCES `tip_prijave` (`vloga`);

--
-- Omejitve za tabelo `potovanje`
--
ALTER TABLE `potovanje`
  ADD CONSTRAINT `potovanje_ibfk_1` FOREIGN KEY (`IDdestinacije`) REFERENCES `destinacija` (`IDdestinacije`);

--
-- Omejitve za tabelo `rezervacija`
--
ALTER TABLE `rezervacija`
  ADD CONSTRAINT `rezervacija_ibfk_2` FOREIGN KEY (`vzdevek`) REFERENCES `oseba` (`vzdevek`),
  ADD CONSTRAINT `rezervacija_ibfk_3` FOREIGN KEY (`IDpotovanja`) REFERENCES `potovanje` (`IDpotovanja`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
