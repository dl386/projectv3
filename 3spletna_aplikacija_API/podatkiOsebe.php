<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Potovalni nacrt - Podatki osebe</title>
		<link rel="stylesheet" type="text/css" href="stil.css" />
	</head>
	<body>
		<div class="center">
			<?php include "Meni.html"?>
		</div>
		<div style="background-image: url('admin.png'); height:85% ; width:99%; position:absolute; background-repeat:no-repeat; background-size:100% 100%">
			<div class="center">
			<form onsubmit="podatkiOsebe(); return false;" id="obrazec">
			
				<label class="pisavaAdmin" for="vzdevekVsi">Vzdevek:</label>
				<input type="text" id="vzdevekVsi" list="options-list" required />
				<input type="submit" value="Prikaži" />				
				<datalist id="options-list">
				</datalist>		
			</form>
			<div class="pisavaAdmin" id="odgovor"></div>
		</div></div>
		<script src="JS/podatkiOsebe.js"></script>
	</body>
</html>