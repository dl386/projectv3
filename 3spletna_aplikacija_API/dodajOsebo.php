<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Potovalni nacrt - Vnos osebe</title>
		<link rel="stylesheet" type="text/css" href="stil.css" />
		
	</head>
	<body>
		<div class="center">
			<?php include "Meni.html"?>
			</div>
			<div style="background-image: url('admin.png'); height:85% ; width:99%; position:absolute; background-repeat:no-repeat; background-size:100% 100%">
			<div class="center">
			<form method="post" id="obrazec" onsubmit="dodajOsebo(); return false;">
				<label class="pisavaAdmin" for="vzdevek">Vzdevek:</label><br>
				<input type="text" name="vzdevek" required/> <br>
				
				<label class="pisavaAdmin" for="geslo">Geslo:</label><br>
				<input type="password" name="geslo" required/> <br>
				
				<label class="pisavaAdmin" for="ime">Ime:</label><br>
				<input type="text" name="ime" required/> <br>

				<label class="pisavaAdmin" for="priimek">Priimek:</label><br>
				<input type="text" name="priimek" required/> <br>
				
				<label class="pisavaAdmin" for="email">Email:</label><br>
				<input type="email" name="email" required/> <br>
				
				<label class="pisavaAdmin" for="vloga">Vloga(stevilka) prijave:</label>
				<label style="background-color:white" for="vloga">(1=admin, 2=agencija, 3=uporabnik):</label><br>
				<input type="vloga" name="vloga" required/> <br>
				
				<input type="submit" value="Dodaj" />
			</form>
			<div id="odgovor" class="pisavaAdmin">
			</div></div>
			</div>
			<script src="JS/dodajOsebo.js"></script>
	</body>
</html>