<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Potovalni nacrt - Registracija uporabnika</title>
		<link rel="stylesheet" type="text/css" href="stil.css" />
		
	</head>
	<body>
		<div class="center">
		
			<form method="post" id="obrazec" onsubmit="dodajOsebo(); return false;">
				<label for="vzdevek">Vzdevek:</label><br>
				<input type="text" name="vzdevek" required/> <br>
				
				<label for="geslo">Geslo:</label><br>
				<input type="password" name="geslo" required/> <br>
				
				<label for="ime">Ime:</label><br>
				<input type="text" name="ime" required/> <br>

				<label for="priimek">Priimek:</label><br>
				<input type="text" name="priimek" required/> <br>
				
				<label for="email">Email:</label><br>
				<input type="email" name="email" required/> <br>
				
				<input type="hidden" name="vloga" value="3"> <br>
				
				<input type="submit" value="Registriraj" />
			</form>
			<div id="odgovor"></div>
			<script src="JS/registracijaUporabnika.js"></script>
		</div>
	</body>
</html>