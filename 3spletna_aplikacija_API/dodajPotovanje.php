<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Potovalni nacrt - Vnos potovanja</title>
		<link rel="stylesheet" type="text/css" href="stil.css" />
	
	</head>
	<body>
		<div class="center">
			<?php include "Meni2.html"?>
			</div>
			<div style="background-image: url('agencija.jpg'); height:85% ; width:99%; position:absolute; background-repeat:no-repeat; background-size:100% 100%">
			<div class="center">
			<form method="post" id="obrazec" onsubmit="dodajPotovanje(); return false;">
			
				<label class="pisavaAdmin" for="IDdestinacije">Stevilka destinacije:</label><br>
				<input type="text" name="IDdestinacije" required/> <br>
				
				<label class="pisavaAdmin" for="datum">Datum:</label><br>
				<input type="text" name="datum" required/> <br>
				
				<label class="pisavaAdmin" for="trajanje">Trajanje(dnevi):</label><br>
				<input type="text" name="trajanje" required/> <br>

				<label class="pisavaAdmin" for="agencija">Agencija:</label><br>
				<input type="text" name="agencija" required/> <br>
				
				<label class="pisavaAdmin" for="cena">Cena:</label><br>
				<input type="number" name="cena" required/> <br>
				
				<label class="pisavaAdmin" for="opis_aranzmaja">Opis aranzmaja:</label><br>
				<input type="text" name="opis_aranzmaja" required/> <br>
				
				<input type="submit" value="Dodaj" />
			</form>
			<div id="odgovor" class="pisavaAdmin">
			</div></div>
			</div>
			<script src="JS/dodajPotovanje.js"></script>
	</body>
</html>